import { Component, OnInit } from '@angular/core';

import { SectionsI } from "../../interfaces/SectionsI";
import { NoticiaService } from '../services/Noticia/noticia.service';
import { SectionsService } from '../services/Sections/sections.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  sectionsConfig = {
    imgSize: 3,
    contentSize: 9
  }

  sections: Array<SectionsI> = [];

  constructor(private sectionsService: SectionsService, private noticiaService: NoticiaService) {
    this.sections = this.sectionsService.getSections();
  }

  ngOnInit() {
  }

  searchTags(e) {
    this.sections = [];
    const busqueda = e.detail.value;
    if (busqueda != "") {
      this.sections = this.sectionsService.filterTags(busqueda);
    }
    else {
      this.sections = this.sectionsService.getSections();
    }
  }

  sendSections(){
    this.noticiaService.setNewSections(this.sections);
  }

}
