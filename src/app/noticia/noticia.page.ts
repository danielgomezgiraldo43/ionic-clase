import { Component, OnInit } from '@angular/core';
// import { type } from 'os';
import { NoticiaService } from 'src/app/services/Noticia/noticia.service';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.page.html',
  styleUrls: ['./noticia.page.scss'],
})
export class NoticiaPage implements OnInit {

  noticiaData=[];

  constructor(private noticiaService: NoticiaService) { }

  ngOnInit() {
    this.getNoticia();
  }

  getNoticia(){
    const queryString = window.location.pathname;
    // console.log((queryString));
    let section=parseInt(queryString.substring(9,10));
    let noticia=parseInt(queryString.substring(11,12));
    // console.log(section,noticia);
    
    let info=this.noticiaService.getNoticia(section,noticia);
    this.noticiaData=info;
    // console.log(this.noticiaData);

  }

}
