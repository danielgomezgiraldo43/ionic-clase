import { Injectable } from '@angular/core';
import { SectionsI } from 'src/interfaces/SectionsI';
import db from "src/db/Sections.json";
import { constants } from 'buffer';
import { NoticiasI } from 'src/interfaces/NoticiasI';

@Injectable({
  providedIn: 'root'
})
export class SectionsService {

  constructor() { }

  getSections(){
    const sections: Array<SectionsI> = db;
    return sections;
  }

  filterTags(e){
    const sections: Array<SectionsI> = db;
    let showSection: Array<SectionsI> = [];
    sections.forEach(section => {
      const noticias = section.body.noticias;
      let sectionHeader = section.header;
      let vNoticias: Array<NoticiasI> = [];
      noticias.forEach(noticia => {
        const tags = noticia.categorias;
        let isPushed: boolean = false;
        tags.forEach(tag => {
          if (tag.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").indexOf(e.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "")) > -1 && !isPushed){
            vNoticias.push(noticia);
            isPushed = true;
          }
        })
      })
      let sectionObject: SectionsI = {
        header: sectionHeader,
        body: {
          noticias: vNoticias
        }
      }
      if (vNoticias.length > 0){
        showSection.push(sectionObject);
      }
    })
    return showSection;
  }
}
