import { Injectable } from '@angular/core';
import db from "src/db/Sections.json";
import { SectionsI } from "../../../interfaces/SectionsI";



@Injectable({
  providedIn: 'root'
})
export class NoticiaService {

  sectionsA: Array<SectionsI> = [];
  constructor() { }

  setNewSections(sections){
    this.sectionsA = sections;
  }

  getNoticia(section: number, noticia:number){
    const sections: Array<SectionsI> = this.sectionsA;
    // console.log(sections)

    for(let i=0;i<sections.length;i++){
      if(i==section){
        for(let j=0;j<sections[i].body.noticias.length;j++){
          if(j==noticia){
            let titulo=sections[i].body.noticias[j].title;
            let img=sections[i].body.noticias[j].img;
            let cont=sections[i].body.noticias[j].content;
            return [titulo,img,cont];
          }
        }
      }
    }
  }
}
