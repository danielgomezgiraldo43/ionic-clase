export interface NoticiasI {
        img: string
        title: string
        content: string
        categorias: Array <string>
        imgSize?: number
        contentSize?: number
}