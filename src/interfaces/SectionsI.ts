import { NoticiasI } from './NoticiasI';

export interface SectionsI {
    header: {
        title: string
        icon?: string
    }
    body: {
        noticias: Array <NoticiasI>
    }
}